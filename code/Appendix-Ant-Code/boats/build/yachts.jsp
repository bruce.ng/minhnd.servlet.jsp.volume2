<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<!--
Taken from Core Servlets and JavaServer Pages Volume II
from Prentice Hall and Sun Microsystems Press,
http://volume2.coreservlets.com/.
(C) 2007 Marty Hall, Larry Brown, and Yaakov Chaikin;
may be freely used or adapted.
-->
<HTML>
<HEAD>
<TITLE>Yachts</TITLE>
<LINK REL=STYLESHEET HREF="app-styles.css" TYPE="text/css">
</HEAD>

<BODY>
  <TABLE BORDER=5 ALIGN="CENTER">
    <TR>
      <TH CLASS="TITLE">Yachts
  </TABLE>
  <P>
    Luxurious models for the <S>wasteful</S> wealthy buyer.
  <H2>Available Models</H2>
  Choose a model to see a picture along with price and availability information.

  <FORM ACTION="DisplayItem">
    <INPUT TYPE="RADIO" NAME="itemNum" VALUE="BM1"> Base Model -- Includes 4-car garage<BR> <INPUT TYPE="RADIO" NAME="itemNum"
      VALUE="MR1"> Mid Range -- Has 15 bedrooms and a helipad<BR> <INPUT TYPE="RADIO" NAME="itemNum" VALUE="HE1"> High End --
    Free tropical island nation included
    <P>
    <CENTER>
      <INPUT TYPE="SUBMIT" VALUE="Get Details">
    </CENTER>
  </FORM>

  <%-- Note the lack of "boats" at the front of URI below --%>
  <%@ taglib uri="/WEB-INF/tlds/count-taglib.tld" prefix="boats"%>
  <boats:count />
</BODY>
</HTML>