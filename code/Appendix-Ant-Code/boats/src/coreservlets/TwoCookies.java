package coreservlets;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

/** Sets two simple cookies and then forwards to the
 *  servlet that displays info on sessions, cookies,
 *  and the servlet context.
 *  <P>
 *  Taken from Core Servlets and JavaServer Pages Volume II
 *  from Prentice Hall and Sun Microsystems Press,
 *  http://volume2.coreservlets.com/.
 *  (C) 2007 Marty Hall, Larry Brown, and Yaakov Chaikin;
 *  may be freely used or adapted.
 */

public class TwoCookies extends HttpServlet {
  public void doGet(HttpServletRequest request,
                    HttpServletResponse response)
      throws ServletException, IOException {
    Cookie c1 = new Cookie("testCookie1", "Cookie One");
    c1.setMaxAge(3600);
    response.addCookie(c1); // Default Path
    Cookie c2 = new Cookie("testCookie2", "Cookie Two");
    c2.setMaxAge(3600);
    c2.setPath("/"); // Explicit Path
    response.addCookie(c2);
    String url = request.getContextPath() +
                 "/servlet/coreservlets.ShowSharedInfo";
    response.sendRedirect(url);
  }
}
