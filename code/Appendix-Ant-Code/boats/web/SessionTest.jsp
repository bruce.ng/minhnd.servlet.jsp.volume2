<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<!--
Taken from Core Servlets and JavaServer Pages Volume II
from Prentice Hall and Sun Microsystems Press,
http://volume2.coreservlets.com/.
(C) 2007 Marty Hall, Larry Brown, and Yaakov Chaikin;
may be freely used or adapted.
-->
<HTML>
<HEAD>
<TITLE>Session Test</TITLE>
<LINK REL=STYLESHEET HREF="app-styles.css" TYPE="text/css">
</HEAD>

<BODY>
  <TABLE BORDER=5 ALIGN="CENTER">
    <TR>
      <TH CLASS="TITLE">Session Test
  </TABLE>
  <H2>
    Session items:
    <%=session.getAttribute("items")%></H2>

</BODY>
</HTML>