bin\certtool.bat setupca -dn "CN=MyCompany Root CA, OU=J2EE Division, O=MyCompany Inc., C=US" -storetype jks -password srvrpass


c:\j2sdk1.4.2_09\bin\keytool -alias tomcat -genkey -keyalg RSA -validity 730 -keystore .keystore -storepass srvrpass


c:\j2sdk1.4.2_09\bin\keytool -certreq -alias tomcat -keystore .keystore -storepass srvrpass -file server.csr


bin\certtool.bat issue -csrfile server.csr -cerfile server.cer -password srvrpass


c:\j2sdk1.4.2_09\bin\keytool -import -alias tomcat -keystore .keystore -storepass srvrpass -file server.cer

c:\j2sdk1.4.2_09\bin\keytool -export -alias cakey -file ca.cer -keystore cadir\ca.ks -storetype jks -storepass certauth

java -jar -Djavax.net.ssl.trustStore=C:\jstk-1.0.1\cadir\ca.ks -Djavax.net.ssl.trustStoreType=jks webclient.jar