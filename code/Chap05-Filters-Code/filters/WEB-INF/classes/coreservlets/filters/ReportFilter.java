
package coreservlets.filters;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

/** Simple filter that prints a report on the standard output 
 *  each time an associated servlet or JSP page is accessed.
 *  <P>
 *  Taken from Core Servlets and JavaServer Pages Volume II
 *  from Prentice Hall and Sun Microsystems Press,
 *  http://volume2.coreservlets.com/.
 *  (C) 2007 Marty Hall, Larry Brown, and Yaakov Chaikin;
 *  may be freely used or adapted.
 */
public class ReportFilter implements Filter {
  
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws ServletException, IOException {
    HttpServletRequest req = (HttpServletRequest) request;
    System.out.println(req.getRemoteHost() + " tried to access " + req.getRequestURL() + " on " + new Date() + ".");
    chain.doFilter(request, response);
  }
  
  public void init(FilterConfig config) {
  }
  
  public void destroy() {
  }
}
