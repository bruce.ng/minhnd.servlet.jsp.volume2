package coreservlets.filters;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

/** StringOutputStream is a stub for ServletOutputStream which
 *  buffers up the output in a string buffer instead of sending it
 *  straight to the client.
 *  <P>
 *  Taken from Core Servlets and JavaServer Pages Volume II
 *  from Prentice Hall and Sun Microsystems Press,
 *  http://volume2.coreservlets.com/.
 *  (C) 2007 Marty Hall, Larry Brown, and Yaakov Chaikin;
 *  may be freely used or adapted.
 */
public class StringOutputStream
             extends ServletOutputStream {
  private StringWriter stringWriter;

  public StringOutputStream(StringWriter stringWriter) {
    this.stringWriter = stringWriter;
  }

  public void write(int c) {
    stringWriter.write(c);
  }
}
