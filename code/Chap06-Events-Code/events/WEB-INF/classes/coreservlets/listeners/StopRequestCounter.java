package coreservlets.listeners;

import javax.servlet.*;

/** Listener that looks for 'stats' as the added attribute
 *  to the request scope. If such an attribute is added, it
 *  signals RequestCounter to stop collecting request
 *  frequency data.
 *  <P>
 *  Taken from Core Servlets and JavaServer Pages Volume II
 *  from Prentice Hall and Sun Microsystems Press,
 *  http://volume2.coreservlets.com/.
 *  (C) 2007 Marty Hall, Larry Brown, and Yaakov Chaikin;
 *  may be freely used or adapted.
 */
public class StopRequestCounter 
       implements ServletRequestAttributeListener {

  /** If the attribute added to the request scope is "stats",
   *  signal to the ServletRequestListener to stop recording
   *  request statistics.
   */
  public void attributeAdded(ServletRequestAttributeEvent event) {
    String attributeName = event.getName();
    if (attributeName.equals("stats")) {
      RequestCounter.setCountingFinished(true);
    }
  }

  public void attributeRemoved(ServletRequestAttributeEvent event) {
  }

  public void attributeReplaced(ServletRequestAttributeEvent event) {
  }
}
