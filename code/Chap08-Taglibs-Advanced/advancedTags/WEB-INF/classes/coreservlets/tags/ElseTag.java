package coreservlets.tags;

import javax.servlet.jsp.*;
import javax.servlet.jsp.tagext.*;
import java.io.*;

/** Tag handler class for the else tag. It gets a hold of
 *  the IfTag instance and processes its body if the value
 *  test attribute of the IfTag is false. It also throws
 *  a JspTagException if the parent of this tag is anything
 *  other than an instance of the IfTag class.
 *  <P>
 *  Taken from Core Servlets and JavaServer Pages Volume II
 *  from Prentice Hall and Sun Microsystems Press,
 *  http://volume2.coreservlets.com/.
 *  (C) 2007 Marty Hall, Larry Brown, and Yaakov Chaikin;
 *  may be freely used or adapted.
 */
public class ElseTag extends SimpleTagSupport {
  public void doTag() throws JspException, IOException {
    // Get parent tag (if tag)
	IfTag ifTag = null;
	try {
	  ifTag = (IfTag)getParent();
	}
	catch (ClassCastException cce) {
	  String msg =
	    "Error: 'else' must be inside 'if'.";
	  throw new JspTagException(msg);
	}

    if (ifTag != null) {
      // Decide whether to output body of else
      if (!ifTag.getTest()) {
        getJspBody().invoke(null);
      }
    } else {
      String msg =
        "Error: 'else' must be inside 'if'.";
      throw new JspTagException(msg);
    }
  }
}
