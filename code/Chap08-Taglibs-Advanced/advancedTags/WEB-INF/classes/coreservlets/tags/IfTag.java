package coreservlets.tags;

import javax.servlet.jsp.*;
import javax.servlet.jsp.tagext.*;
import java.io.*;

/** Tag handler class for the if tag. It relies on the
 *  required 'test' attribute and stores the evaluated
 *  condition in the test instance variable to be later
 *  accessed by the ThenTag.java and ElseTag.java.
 *  <P>
 *  Taken from Core Servlets and JavaServer Pages Volume II
 *  from Prentice Hall and Sun Microsystems Press,
 *  http://volume2.coreservlets.com/.
 *  (C) 2007 Marty Hall, Larry Brown, and Yaakov Chaikin;
 *  may be freely used or adapted.
 */
public class IfTag extends SimpleTagSupport {
  private boolean test;

  public void setTest(boolean test) {
    this.test = test;
  }

  public boolean getTest() {
    return(test);
  }

  public void doTag() throws JspException, IOException {
    getJspBody().invoke(null);
  }
}
