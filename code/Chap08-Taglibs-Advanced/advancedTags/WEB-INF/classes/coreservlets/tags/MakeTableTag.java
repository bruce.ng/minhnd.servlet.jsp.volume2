package coreservlets.tags;

import javax.servlet.jsp.*;
import javax.servlet.jsp.tagext.*;
import java.io.*;

/** Tag handler class for the makeTable tag. It builds an
 *  HTML table and outputs the records of the two
 *  dimensional array provided as one of the attributes of
 *  the tag in the JSP page.
 *  <P>
 *  Taken from Core Servlets and JavaServer Pages Volume II
 *  from Prentice Hall and Sun Microsystems Press,
 *  http://volume2.coreservlets.com/.
 *  (C) 2007 Marty Hall, Larry Brown, and Yaakov Chaikin;
 *  may be freely used or adapted.
 */
public class MakeTableTag extends SimpleTagSupport {
  private Object[][] rowItems;
  private String headerClass;
  private String bodyClass;

  public void setRowItems(Object[][] rowItems) {
    this.rowItems = rowItems;
  }

  public void setHeaderClass(String headerClass) {
    this.headerClass = headerClass;
  }

  public void setBodyClass(String bodyClass) {
    this.bodyClass = bodyClass;
  }

  public void doTag() throws JspException, IOException {
    if (rowItems.length > 0) {
      JspContext context = getJspContext();
      JspWriter out = context.getOut();
      out.println("<TABLE BORDER=1>");
      Object[] headingRow = rowItems[0];
      printOneRow(headingRow, getStyle(headerClass), out);
      for(int i=1; i<rowItems.length; i++) {
        Object[] bodyRow = rowItems[i];
        printOneRow(bodyRow, getStyle(bodyClass), out);
      }
      out.println("</TABLE>");
    }
  }

  private void printOneRow(Object[] columnEntries,
                           String style,
                           JspWriter out)
      throws IOException {
    out.println("  <TR" + style + ">");
    for(int i=0; i<columnEntries.length; i++) {
      Object columnEntry = columnEntries[i];
      out.println("    <TD>" + columnEntry + "</TD>");
    }
    out.println("  </TR>");
  }

  private String getStyle(String className) {
    if (className == null) {
      return("");
    } else {
      return(" CLASS=\"" + headerClass + "\"");
    }
  }
}
