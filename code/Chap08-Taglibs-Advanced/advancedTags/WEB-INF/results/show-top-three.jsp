<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
<TITLE>Results</TITLE>
<LINK REL=STYLESHEET HREF="JSP-Styles.css" TYPE="text/css">
</HEAD>
<BODY>
  <H1>Results</H1>
  <%@ taglib uri="/WEB-INF/tlds/csajsp-taglib-adv.tld" prefix="csajsp"%>
  <H2>First Place</H2>
  <csajsp:showName name="${topThree[0]}" />
  <H2>Second Place</H2>
  <csajsp:showName name="${topThree[1]}" />
  <H2>Third Place</H2>
  <csajsp:showName name="${topThree[2]}" />
</BODY>
</HTML>