package coreservlets;

import javax.servlet.http.*;

/** Action that stores the warning messages in the form bean.
 *  This approach allows displaying the input form values
 *  along with error messages on the input page.
 *  <P>
 *  Taken from Core Servlets and JavaServer Pages Volume II
 *  from Prentice Hall and Sun Microsystems Press,
 *  http://volume2.coreservlets.com/.
 *  (C) 2007 Marty Hall, Larry Brown, and Yaakov Chaikin;
 *  may be freely used or adapted.
 */

public class SignupAction2 extends SignupAction1 {
  protected void makeWarning(HttpServletRequest request,
                             String message) {
    ContactFormBean contactFormBean =
      (ContactFormBean)request.getAttribute("contactFormBean");
    contactFormBean.setWarning(message);
  }
}
