package coreservlets;

import javax.servlet.http.*;
import org.apache.struts.action.*;

/** An action that uses an ActionForm bean to hold the HTML
 *  form parameters. Upon submission of the HTML form, the
 *  Struts system automatically populates the email and
 *  password fields of the UserFormBean.
 *  <P>
 *  Taken from Core Servlets and JavaServer Pages Volume II
 *  from Prentice Hall and Sun Microsystems Press,
 *  http://volume2.coreservlets.com/.
 *  (C) 2007 Marty Hall, Larry Brown, and Yaakov Chaikin;
 *  may be freely used or adapted.
 */

public class BeanRegisterAction extends Action {
  public ActionForward execute(ActionMapping mapping,
                               ActionForm form,
                               HttpServletRequest request,
                               HttpServletResponse response)
      throws Exception {
    UserFormBean userBean = (UserFormBean)form;
    String email = userBean.getEmail();
    String password = userBean.getPassword();
    SuggestionBean suggestionBean =
      SuggestionUtils.getSuggestionBean();
    request.setAttribute("suggestionBean", suggestionBean);
    if ((email == null) ||
        (email.trim().length() < 3) ||
        (email.indexOf("@") == -1)) {
      return(mapping.findForward("bad-address"));
    } else if ((password == null) ||
               (password.trim().length() < 6)) {
      return(mapping.findForward("bad-password"));
    } else {
      return(mapping.findForward("success"));
    }
  }
}
