package coreservlets;

import org.apache.struts.action.*;

/** A FormBean for registration information. When the user
 *  submits the input form, the Struts system automatically
 *  populates the bean with the values from the email and
 *  password request parameters.
 *  <P>
 *  Taken from Core Servlets and JavaServer Pages Volume II
 *  from Prentice Hall and Sun Microsystems Press,
 *  http://volume2.coreservlets.com/.
 *  (C) 2007 Marty Hall, Larry Brown, and Yaakov Chaikin;
 *  may be freely used or adapted.
 */

public class UserFormBean extends ActionForm {
  private String email = "Missing address";
  private String password = "Missing password";

  public String getEmail() {
    return(email);
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getPassword() {
    return(password);
  }

  public void setPassword(String password) {
    this.password = password;
  }
}
