<%@ taglib uri="http://struts.apache.org/tags-html"
           prefix="html" %>
<!--
Taken from Core Servlets and JavaServer Pages Volume II
from Prentice Hall and Sun Microsystems Press,
http://volume2.coreservlets.com/.
(C) 2007 Marty Hall, Larry Brown, and Yaakov Chaikin;
may be freely used or adapted.
-->
Stable and roomy models for the <S>uninformed</S>
innovative buyer.
<H2>Available Models</H2>
Choose a model to see a picture along with price and
availability information.
<FORM ACTION="<html:rewrite action='/actions/displayItem'/>">
<INPUT TYPE="RADIO" NAME="itemNum" VALUE="Valdez">
Valdez -- Slightly damaged model available at discount<BR>
<INPUT TYPE="RADIO" NAME="itemNum" VALUE="BigBertha">
Big Bertha -- Includes 10 million gallon swimming pool<BR>
<INPUT TYPE="RADIO" NAME="itemNum" VALUE="EcoDisaster">
ED I -- For those who don't mind political incorrectness
<P>
<CENTER>
<INPUT TYPE="SUBMIT" VALUE="Get Details">
</CENTER>
</FORM>
<CENTER>
<IMG SRC="<html:rewrite page='/images/tanker.jpg'/>"
     ALT="Tanker"></CENTER>