<%@ taglib uri="http://struts.apache.org/tags-tiles"
           prefix="tiles" %>
<!--
Taken from Core Servlets and JavaServer Pages Volume II
from Prentice Hall and Sun Microsystems Press,
http://volume2.coreservlets.com/.
(C) 2007 Marty Hall, Larry Brown, and Yaakov Chaikin;
may be freely used or adapted.
-->
<tiles:insert page="/WEB-INF/tiles/layout.jsp">
  <tiles:put name="title" value="Page 2 Title"/>
  <tiles:put name="header" value="/WEB-INF/tiles/header.jsp"/>
  <tiles:put name="menu" value="/WEB-INF/tiles/menu.jsp"/>
  <tiles:put name="body" value="/WEB-INF/tiles/body2.jsp"/>
  <tiles:put name="footer" value="/WEB-INF/tiles/footer.jsp"/>
</tiles:insert>