package coreservlets;

import javax.servlet.http.*;
import org.apache.struts.action.*;

/** Action that performs manual validation of the form bean
 *  properties. If a problem exists with the user provided data,
 *  then the Action creates warning messages and place them in
 *  request scope.
 *  <P>
 *  Taken from Core Servlets and JavaServer Pages Volume II
 *  from Prentice Hall and Sun Microsystems Press,
 *  http://volume2.coreservlets.com/.
 *  (C) 2007 Marty Hall, Larry Brown, and Yaakov Chaikin;
 *  may be freely used or adapted.
 */

public class ShowSampleAction extends Action {
  public ActionForward execute(ActionMapping mapping,
                               ActionForm form,
                               HttpServletRequest request,
                               HttpServletResponse response)
      throws Exception {
    FormatFormBean formatBean = (FormatFormBean)form;
    ActionForward forward = mapping.findForward("success");
    ActionMessages messages = new ActionMessages();

    if (formatBean.isMissing(formatBean.getTitleSize())) {
      addMessage(messages, "title", "Missing Title Size");
      forward = mapping.findForward("missing-data");
    }
    if (formatBean.isMissing(formatBean.getHeadingSize())) {
      addMessage(messages, "headings", "Missing Heading Size");
      forward = mapping.findForward("missing-data");
    }
    if (formatBean.isMissing(formatBean.getBodySize())) {
      addMessage(messages, "body", "Missing Body Size");
      forward = mapping.findForward("missing-data");
    }
    if (formatBean.isMissing(formatBean.getBgColor())) {
      addMessage(messages, "bg", "Missing Background Color");
      forward = mapping.findForward("missing-data");
    }
    if (formatBean.isMissing(formatBean.getFgColor())) {
      addMessage(messages, "fg", "Missing Foreground Color");
      forward = mapping.findForward("missing-data");
    } else if (formatBean.getFgColor().equals
                 (formatBean.getBgColor())) {
      addMessage(messages, "fg",
                 "Foreground and Background Identical!");
      forward = mapping.findForward("missing-data");
    }
    if (!messages.isEmpty()) {
      addMessages(request, messages);
    }
    return(forward);
  }

  protected void addMessage(ActionMessages messages,
                            String property,
                            String warning) {
     messages.add(property, new ActionMessage(warning, false));
  }
}
