<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<!--
Taken from Core Servlets and JavaServer Pages Volume II
from Prentice Hall and Sun Microsystems Press,
http://volume2.coreservlets.com/.
(C) 2007 Marty Hall, Larry Brown, and Yaakov Chaikin;
may be freely used or adapted.
-->
<HTML>
<HEAD><TITLE>Choose Format</TITLE></HEAD>
<BODY BGCOLOR="#FDF5E6">
<H1 ALIGN="CENTER">Choose Format</H1>
Please select the font sizes and colors that you would
like used to display your resume.
<P>
<CENTER>
<%@ taglib uri="http://struts.apache.org/tags-html"
           prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean"
           prefix="bean" %>
<html:form action="/actions/showSample2">
  <bean:write name="formatFormBean"
              property="warning" filter="false"/><BR>
  Title size: <html:text property="titleSize"/><BR>
  Heading size: <html:text property="headingSize"/><BR>
  Body text size: <html:text property="bodySize"/><BR>
  Background color: <html:text property="bgColor"/><BR>
  Foreground color: <html:text property="fgColor"/><BR>
  <html:submit value="Show Sample"/>
</html:form>
</CENTER>
</BODY></HTML>