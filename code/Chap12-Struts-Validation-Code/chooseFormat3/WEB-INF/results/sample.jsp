<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<!--
Taken from Core Servlets and JavaServer Pages Volume II
from Prentice Hall and Sun Microsystems Press,
http://volume2.coreservlets.com/.
(C) 2007 Marty Hall, Larry Brown, and Yaakov Chaikin;
may be freely used or adapted.
-->
<HTML>
<HEAD><TITLE>Sample</TITLE>
<%@ taglib uri="http://struts.apache.org/tags-bean"
           prefix="bean" %>
<bean:write name="formatFormBean" property="styleSheet"
            filter="false"/>
</HEAD>
<BODY>
<H1 ALIGN="CENTER">Your Name Here</H1>
Intro. Blah blah blah. Yadda, yadda, yadda.
<H2>Professional Experience</H2>
<UL>
  <LI>Blah blah blah.
  <LI>Yadda, yadda, yadda.
</UL>
<H2>Education</H2>
Blah blah blah. Yadda, yadda, yadda.
</BODY></HTML>