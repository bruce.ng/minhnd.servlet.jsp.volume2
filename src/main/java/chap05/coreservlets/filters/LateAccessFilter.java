package chap05.coreservlets.filters;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import chap05.StringOutputStream;
import chap05.StringWrapper;

/** Filter that keeps track of accesses that occur at unusual hours. */
public class LateAccessFilter implements Filter {

  private FilterConfig config;
  private ServletContext context;
  private int startTime, endTime;
  private DateFormat formatter;
  
  @Override
  public void destroy() {
     
  }

  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
    HttpServletRequest req = (HttpServletRequest) request;
    GregorianCalendar calendar = new GregorianCalendar();
    int currentTime = calendar.get(Calendar.HOUR_OF_DAY);
    System.out.println("currentTime==" + currentTime);
    if (isUnusualTime(currentTime, startTime, endTime)) {
      //System.out.println("startTime==" + startTime + "==endTime==" + endTime);
      //dir_tomcat/logs
      context.log("WARNING: " + req.getRemoteHost() + " accessed " + req.getRequestURL() + " on " + formatter.format(calendar.getTime()));
    }
    //PrintWriter writer = response.getWriter();
    //ServletOutputStream outputStream = response.getOutputStream();
    
    
    StringWrapper sWrapper = new StringWrapper((HttpServletResponse) response);
    PrintWriter writer = sWrapper.getWriter();
    ServletOutputStream outputStream = sWrapper.getOutputStream();
    
    String string = sWrapper.toString();
    chain.doFilter(request, response);
  }
  
//  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws ServletException, IOException {
//    HttpServletRequest req = (HttpServletRequest) request;
//    HttpServletResponse res = (HttpServletResponse) response;
//    if (isUnusualCondition(req)) {
//      res.sendRedirect("http://www.somesite.com");
//    } else {
//      chain.doFilter(req, res);
//    }
//  }

  @Override
  public void init(FilterConfig config) throws ServletException {
    this.config = config;
    context = this.config.getServletContext();
    formatter = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM);
    try {
      startTime = Integer.parseInt(config.getInitParameter("startTime"));
      endTime = Integer.parseInt(config.getInitParameter("endTime"));
    } catch (NumberFormatException nfe) {
      // Malformed or null
      // Default: access at or after 10 p.m. but before 6 a.m. is considered
      // unusual.
      startTime = 22; // 10:00 p.m.
      endTime = 6; // 6:00 a.m.
    }
  }
  
  // Is the current time between the start and end times that are marked as abnormal access times?
  private boolean isUnusualTime(int currentTime, int startTime, int endTime) {
    // If the start time is less than the end time (i.e., they are two times on the same day),
    // then the current time is considered unusual if it is between the start and end times.
    if (startTime < endTime) {
      return ((currentTime >= startTime) && (currentTime < endTime));
    }
    // If the start time is greater than or equal to the end time (i.e., the start time is on one day and the end time is on the next day),
    // then the current time is considered unusual if it is NOT between the end and start times.
    else {
      return (!isUnusualTime(currentTime, endTime, startTime));
    }
}
}
