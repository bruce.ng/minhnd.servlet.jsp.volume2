package chap05.coreservlets.filters;

import java.io.IOException;
import java.util.Date;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

public class ReportFilter implements Filter {

 

  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
    HttpServletRequest req = (HttpServletRequest) request;
    System.out.println("ReportFilter==" + req.getRemoteHost() + " tried to access " + req.getRequestURL() + " on " + new Date() + ".");
    chain.doFilter(request, response);
  }

  @Override
  public void init(FilterConfig config) throws ServletException {
    String filterName = config.getFilterName();
    System.out.println("filterName==" + filterName);
    String initParameter = config.getInitParameter("binhminh");
    System.out.println("initParameter==" + initParameter);
  }
  
  @Override
  public void destroy() {
    
  }
}
