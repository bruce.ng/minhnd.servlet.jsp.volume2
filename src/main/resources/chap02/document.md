/** Controlling Web Application Behavior With Web.xml * */
2.11 Controlling Session Timeouts

There are two small inconsistencies between the session-config method of setting the session timeout and using the setMaxInactiveInterval method of HttpSession .

First, the value of the session-timeout subElement is specified in minutes, whereas the value of setMaxInactiveInterval is specified in seconds.

Second, if session-timeout is specified as either 0 or a negative number, the session will never expire, but only the negative number passed to setMaxInactiveInterval will accomplish the same result.

2.17 Developing for the Clustered Environment
1. Avoid instance variables and static data (such as singletons) for shared data.
2. Don't store data in the ServletContext.
3. Objects stored in HttpSessionmust implement Serializable.
4. Only minimal information should be stored in HttpSession.



