<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
<TITLE>Session Info</TITLE>
<LINK REL=STYLESHEET HREF="events-styles.css" TYPE="text/css">
</HEAD>
<BODY>
  <TABLE BORDER=5 ALIGN="CENTER">
    <TR>
      <TH CLASS="TITLE">Session Info
  </TABLE>
  <P>
  <UL>
    <LI>Total number of sessions in the life of this Web application: ${sessionCounter.totalSessionCount}.
    <LI>Number of sessions currently in memory: ${sessionCounter.currentSessionCount}.
    <LI>Maximum number of sessions that have ever been in memory at any one time: ${sessionCounter.maxSessionCount}.
  </UL>
</BODY>
</HTML>